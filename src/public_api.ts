/*
 * Public API Surface of universis-events
 */

export * from './events.service';
export { EventsModule } from './events.module';
export * from './components/events/events.component';
export * from './components/new-event/new-event.component';
export * from './components/new-event-routed/new-event-routed.component';
export * from './components/new-event/new-event-container.component';
export * from './components/absences/absences.component';
export * from './components/event-absences/event-absences.component';
export * from './components/upcoming-events/upcoming-events.component';
export * from './advanced-date.formatter';
export * from './advanced-absence.formatter';
export * from './advanced-status.formatter';
export * from './components/delete-event/delete-event.component';
export * from './components/timetable/timetable.component';
export * from './components/timetable/timetable-copy.component';
export * from './components/timetable/timetable-new-edit/timetable-new-edit.component';
export * from './components/timetable/timetable-home/timetable-home.component';
export * from './components/timetable/timetable-root/timetable-root.component';
export * from './components/timetable/timetable-dashboard/timetable-dashboard.component';
export * from './components/timetable/timetable-dashboard/timetable-dashboard-classes/timetable-dashboard-classes.component';
export * from './components/timetable/timetable-dashboard/timetable-dashboard-overview/timetable-dashboard-overview.component';
export * from './components/timetable/timetable-dashboard/timetable-dashboard-overview/timetable-dashboard-overview-actions/timetable-dashboard-overview-actions.component';
export * from './components/timetable/timetable-dashboard/timetable-dashboard-overview/timetable-dashboard-overview-general/timetable-dashboard-overview-general.component';
export * from './components/timetable/timetable-dashboard/timetable-dashboard-overview/timetable-dashboard-overview-stats/timetable-dashboard-overview-stats.component';
