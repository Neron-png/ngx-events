import { AdvancedColumnFormatter } from '@universis/ngx-tables';
import {ElementRef, Input} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

export class AbsenceFormatter extends AdvancedColumnFormatter {
    private _elementRef: ElementRef;
    render(data, type, row, meta) {
      const column = meta.settings.aoColumns[meta.col];
      this._elementRef = this.injector.get(ElementRef);
      const translateService: TranslateService = this.injector.get(TranslateService);
      if (!row.hasOwnProperty('absent')) {
        row = {...row, absent: false};
      }
      if (column && column.data) {
        const option1: any = {};
        const option2: any = {};
        const checked = shouldBeChecked => shouldBeChecked ? 'checked' : '';

        if (row.presenceType === 0) {
          option1.name = translateService.instant('Yes');
          option2.name = translateService.instant('No');
          option1.checked = checked(row.absent);
          option2.checked = checked(!row.absent);
          option1.class = row.absent ? 'btn btn-outline-info active' : 'btn btn-outline-info';
          option2.class = row.absent ? 'btn btn-outline-secondary' : 'btn btn-outline-secondary active';
        } else {
          option1.name = translateService.instant('No');
          option2.name = translateService.instant('Yes');
          option1.checked = checked(!row.absent);
          option2.checked = checked(row.absent);
          option1.class = row.absent ? 'btn btn-outline-secondary' : 'btn btn-outline-secondary active';
          option2.class = row.absent ? 'btn btn-outline-info active' : 'btn btn-outline-info';
        }
        // In order to handle overlapping borders within components, on hover, focus or active, bootstrap brings
        // the particular element to the forefront with a higher z-index value to show their border over the sibling elements.
        // source: https://getbootstrap.com/docs/5.0/layout/z-index/

        // The above practice, leads to the active buttons being displayed on top of the toast message. The custom style prevents this.
        const toggle = `
              <span>
                  <span id="${row.student}" class="btn-group btn-group-toggle" data-toggle="buttons">
                    <label id="leftButton" class="${option1.class}" style="z-index: 0 !important; border-right:none;">
                      <input type="radio" ${option1.checked}> ${option1.name}
                    </label>
                    <label id="rightButton" class="${option2.class}" style="z-index: 0 !important; border-left:none;">
                      <input type="radio" ${option2.checked}> ${option2.name}
                    </label>
                  </span>
              </span>
        `;
        return toggle;
      }
      return data;
    }
    constructor( ) {
      super();
    }
  }
