import {Component, Injector, OnInit} from '@angular/core';
import {AppEventService, ErrorService, LoadingService, ModalService, ToastService} from '@universis/common';
import {TranslateService} from '@ngx-translate/core';
import {AngularDataContext} from '@themost/angular';
import {ActivatedRoute, Router} from '@angular/router';
import {combineLatest, Subscription} from 'rxjs';
import { template } from 'lodash';

@Component({
  selector: 'universis-delete-event',
  template: ``
})
export class DeleteEventComponent implements OnInit {
  constructor(private _modalService: ModalService,
              private _translateService: TranslateService,
              private _loadingService: LoadingService,
              private _context: AngularDataContext,
              private _injector: Injector,
              private _errorService: ErrorService) {
  }

  private _router: Router;
  private _activatedRoute: ActivatedRoute;
  private _toastService: ToastService;
  private _paramSubscription: Subscription;
  private _appEventService: AppEventService;
  private _model = '';

  ngOnInit() {
    this._router = this._injector.get(Router);
    this._activatedRoute = this._injector.get(ActivatedRoute);
    this._toastService = this._injector.get(ToastService);
    this._appEventService = this._injector.get(AppEventService);
    this._paramSubscription = combineLatest(
      this._activatedRoute.params,
      this._activatedRoute.data).subscribe(([params, data]) => {
      if (this._model === '') {
        this._model = template(data._model)(params);
      }
      this._modalService.showWarningDialog(this._translateService.instant('Events.Warning'),
        this._translateService.instant('Events.DeleteEvent'))
        .then(async result => {
          if (result === 'ok') {
            this._loadingService.showLoading();
            try {
              await this._context.model(this._model).remove([]);
              this._loadingService.hideLoading();
              this._toastService.show(this._translateService.instant('Events.ActionCompleted'),
                this._translateService.instant('Events.MarkedDeleted'), true, 2000);
            } catch (err) {
              console.log(err);
              this._loadingService.hideLoading();
              this._errorService.showError(err, {
                continueLink: '.',
              });
            }
          }
          return this._router.navigate(['../..'], { relativeTo: this._activatedRoute}).then(navigationEnd => {
            if (navigationEnd && result === 'ok') {
              return this._router.navigate(['.'], {
                relativeTo: this._activatedRoute,
                fragment: 'reload',
                skipLocationChange: true
              });
            } else {
              return Promise.resolve(navigationEnd);
            }
          });
        });

    });
  }
}
