/* tslint:disable quotemark */
/* tslint:disable max-line-length */
export const en = {
  "Tables": {
    "Events": "Events",
    "Event": "Event",
    "Students": "Students",
    "Student": "Student",
    "Classes": "Classes",
    "Class": "Class"
  },
  "Title": "Title",
  "Performers": "Performers",
  "From": "From",
  "To": "To",
  "Total": "Total",
  "More": "More",
  "Actions": "Actions",
  "Statistics": "Timetable Statistics",
  "New": "New",
  "Yes": "Yes",
  "No": "No",
  "OK": "OK",
  "Cancel": "Cancel",
  "Date": "Date",
  "Time": "Time",
  "Name": "Event Name",
  "Last Name": "Last Name",
  "Student Name": "Student Name",
  "Father Name": "Father Name",
  "Student Identifier": "Student Identifier",
  "RegistrationSemester": "Semester",
  "Attendances": "Attendances",
  "Description": "Description",
  "Status": "Status",
  "NumberOfAbsences": "Absences",
  "Performer": "Organizer",
  "Sections": "Sections",
  "StudentClassStatus": "Class Registration Status",
  "TimetableEvents": {
    "TimeFrame": "Timetable Period",
    "Title": "Timetables",
    "TitleSingular": "Timetable",
    "All": "All",
    "Open": "Open",
    "Preview": "Preview",
    "Edit": "Edit",
    "New": "New Timetable",
    "Overview": "Overview",
    "EventTypes": "Available Event Types",
    "NoEventTypes": "No available event types found",
    "Copy": "Copy",
    "DeleteAction": {
      "Title": "Delete timetables",
      "Description": "The operation will try to delete the selected timetables. The operation will fail if there are any completed events or attendance records have been submitted."
    }
  },
  "CopyAction": "Copy action",
  "TimetableCopyAction": {
    "Title": "Timetable copy actions",
    "Failures": "Failures"
  },
  "Events": {
    "IndividualEvents": "Individual events",
    "RecursiveEvents": "Recursive events",
    "RecursiveChildren": "Sub-events",
    "NoUpcomingEventsFound": "No upcoming events were found.",
    "UpcomingEvents": "Upcoming Events",
    "show": "Show",
    "newSimpleEvent": "New event",
    "newRecursiveEvent": "New recursive event",
    "AllPresent": "All present",
    "DeleteAbsences": "Delete absences",
    "DeleteAbsencesTooltip": "With this action, you are going to delete all temporarily saved absences.",
    "AllPresentTooltip": "With this action, you are going to mark all students as present disregarding any previous selection.",
    "Save": "Save",
    "SaveTooltip": "With this action, you are going to save the absence records you set. It's recommended to use this action, when you want to come back to this presence list and set more absence records.",
    "Commit": "Submit",
    "CommitTooltip": "With his action, you are going to save and finalize all the set absence records. You will not be able to edit them and they will appear to the students.",
    "Back": "Back to Events",
    "CloseEvent": "Close Event",
    "OpenEvent": "Open Event",
    "ActionCompleted": "Action Completed",
    "MarkedCompleted": "Marked current event as completed",
    "AllSavedPresent": "Marked and saved all students as present",
    "CreatedNew": "New Event",
    "CreatedSuccess": "New event successfully created",
    "Warning": "Warning",
    "AllPresentCheck": "This action saves all students in this event as present. Are you sure you want to proceed?",
    "DeleteAbsencesCheck": "With this action, you are going to delete all temporarily saved student-absences. Are you sure you want to proceed?",
    "SavedPresent": "Present students saved",
    "SavedAbsent": "Absent students saved",
    "Absent": "Absent",
    "Present": "Mark as present",
    "CommitCheck": "This action cannot be reversed. Are you sure you want to proceed?",
    "Month": "Month",
    "Week": "Week",
    "Day": "Day",
    "OldClassMessage": "The table below displays all the events of the class. You cannot create a new event or delete an existent because the course class belongs to a previous year.",
    "AbsencesMessages": {
      "ShouldSave": "The status of {{numOfStudents}} students has been modified. Press save to register the changes.",
      "ViewMode": "The table below represents the final state of the attendance sheet. You cannot perform any further modifications because the event isn't open or the course class belongs to a previous year.",
      "General": "The table below lists all the students that participate in the event. If you modify the status of any student, you can temporarily save the changes and continue later. When you decide to finalize the attendance sheet you can press submit."
    },
    "This": {
      "Day": "Today",
      "Month": "This month",
      "Week": "This week"
    },
    "Next": {
      "Day": "Tomorrow",
      "Month": "Next month",
      "Week": "Next week"
    },
    "Previous": {
      "Day": "Yesterday",
      "Month": "Previous month",
      "Week": "Previous week"
    },
    "Export": "Export",
    "EventCompleted": "Completed event",
    "EventOpened": "Open event",
    "Statuses": {
      "EventCompleted": "Completed",
      "EventOpened": "Open",
      "EventCancelled": "Cancelled",
      "EventPostponed": "Postponed",
      "EventRescheduled": "Rescheduled",
      "EventScheduled": "Scheduled",
    },
    "ActionTypes": {
      "PotentialActionStatus": "Potential",
      "FailedActionStatus": "Failed",
      "CompletedActionStatus": "Completed",
      "ActiveActionStatus": "Active",
      "CancelledActionStatus": "Cancelled",
      "null": "-"
    },
    "OpenAction": {
      "Title": "Open teaching event",
      "Description": "This action is going to change the status of the selected teaching events to open. An open teaching event can be modified by the instructors and it will be visible both in the students' and the instructors' calendar."
    },
    "CloseAction": {
      "Title": "Close teaching event",
      "Description": "This action is going to change the status of the selected teaching events to completed. A completed teaching event cannot be modified by the instructors and they won't be able to add attendance records for that event."
    },
    "Student": {
      "FamilyName": "Family Name",
      "GivenName": "Given Name",
      "Identifier": "Student Identifier"
    },
    "DeleteEvent": "Deletion action cannot be reversed. Are you sure you want to proceed?",
    "MarkedDeleted": "The event was successfully deleted.",
    "PresenceList": "Presence List",
    "Events": "Teaching Events",
    "Name": "Event name",
    "Sections": "Class section",
    "EventsBefore": "Start date before",
    "EventsAfter": "Start date after",
    "Status": "Event status",
    "SubmitError": "Submission Error",
    "Errors": {
      "Instructor not a course class section instructor": "No access allowed in some of the selected class sections."
    }
  },
  "Places": {
    "AvailablePlaces": "Available Places",
    "NoPlaces": "No available places found",
    "Title": "Places",
    "TitleSingular": "Place",
    "Capacity": "Capacity",
    "ParentPlace": "Parent Place"
  },
  "Classes": {
    "Title": "Class title",
    "ClassEvents": "Class's events",
    "InstructorEvents": "Instructor's events"
  },
  "Instructors": {
    "Instructor": "Instructor",
    "Name": "Instructor's Name",
    "Role": "Role",
    "TeachingHours": "Teaching Hours",
    "Title": "Instructors",
    "TitleSingular": "Instructor",
    "FamilyName": "Family Name",
    "GivenName": "Given Name",
    "Category": "Category",
    "Email": "Email",
    "HomeAddress": "Home Address",
    "HomePhone": "Home Phone",
    "Active": "Active",
    "Inactive": "Inactive",
    "All": "All",
    "New": "New",
    "Department": "Department",
    "WorkPhone": "Work Phone",
    "FullName": "Full Name",
    "MiddleName": "Middle Name",
    "Status": "Status",
    "Statuses": {
      "active": "Active",
      "inactive": "Inactive",
      "suspended": "Suspended",
      "null": "-"
    },
    "Roles": {
      "supervisor": "Supervisor",
      "co-teacher": "Co-teacher",
      "assistant": "Assistant"
    }
  }
};
/* tslint:enable max-line-length */
/* tslint:enable quotemark */
