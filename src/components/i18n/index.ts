import {en} from './events.en';
import {el} from './events.el';

export const EVENTS_LOCALES = {
    el: el,
    en: en
};
