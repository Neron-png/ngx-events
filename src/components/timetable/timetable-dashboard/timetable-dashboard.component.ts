import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { AppEventService } from '@universis/common';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-timetable-dashboard',
  templateUrl: './timetable-dashboard.component.html'
})
export class TimetableDashboardComponent implements OnInit, OnDestroy {

  public model: any;
  public tabs: any[];
  private maxExtraTabs = 3;
  private fixedTabs: number;
  private paramSubscription: Subscription;
  private changeSubscription: Subscription;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _context: AngularDataContext,
    private _appEvent: AppEventService,
  ) { }

  ngOnInit() {
    this.paramSubscription = this._activatedRoute.params.subscribe(async ({ timetable }) => {
      this.model = await this._context.model('TimetableEvents')
        .where('id').equal(timetable)
        .expand('availableEventTypes')
        .getItem();

      const eventTypes = this.model.availableEventTypes.map(type => type.alternateName);
      this.tabs = this._activatedRoute.routeConfig.children.filter(route => {
        if (typeof route.redirectTo === 'undefined' && route.data.title) {
          if (route.data.model === 'TeachingEvent' || route.data.model === 'ExamEvent') {
            return eventTypes.includes(route.data.model);
          }
          return true;
        }
      });
      this.fixedTabs = this.tabs.length;
    });

    // add extra tabs for previewing events and organize them in a first-in-first-out manner
    this.changeSubscription = this._appEvent.changed.subscribe(async (event) => {
      if (event && event.tab) {
        if (!this.tabs.find(tab => tab.path === event.tab.path)) {
          // add tab at the beginning of the extra tabs
          this.tabs.splice(this.fixedTabs, 0, event.tab);

          // if the allowed number of extra tabs is exceeded, remove the last one
          const extraTabs = this.tabs.length - this.fixedTabs;
          if (extraTabs > this.maxExtraTabs) {
            this.tabs.pop();
          }
        }
      }
    });
  }

  ngOnDestroy(): void {
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
    if (this.changeSubscription) {
      this.changeSubscription.unsubscribe();
    }
  }

}
