import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-timetable-dashboard-overview',
  templateUrl: './timetable-dashboard-overview.component.html'
})
export class TimetableDashboardOverviewComponent implements OnInit, OnDestroy {

  public event: any;
  private paramSubscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute, private _context: AngularDataContext) { }

  async ngOnInit() {
    this.paramSubscription = this._activatedRoute.params.subscribe(({timetable}) => {
      this._context.model('TimetableEvents')
        .where('id').equal(timetable)
        // tslint:disable-next-line: max-line-length
        .expand('availablePlaces($select=id,name,maximumAttendeeCapacity as capacity,containedInPlace/name as containedInPlace),availableEventTypes')
        .getItem()
        .then(result => this.event = result);
    });
  }

  ngOnDestroy(): void {
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
  }

}
