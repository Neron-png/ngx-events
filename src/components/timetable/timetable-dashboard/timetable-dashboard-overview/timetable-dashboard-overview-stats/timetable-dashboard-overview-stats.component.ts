import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { AngularDataContext } from '@themost/angular';

@Component({
  selector: 'app-timetable-dashboard-overview-stats',
  templateUrl: './timetable-dashboard-overview-stats.component.html'
})
export class TimetableDashboardOverviewStatsComponent implements OnChanges {

  @Input() public event: any;

  constructor(private _context: AngularDataContext) { }
  public eventSummary;

  async ngOnChanges(changes: SimpleChanges) {
    if (changes.event) {
      if (changes.event.currentValue == null) {
        this.eventSummary = null;
        return;
      }

      const events = await this._context.model('Events')
        .where('superEvent').equal(this.event.id)
        .or('superEvent/superEvent').equal(this.event.id)
        .select('id, superEvent, eventHoursSpecification, performer, eventStatus/alternateName as eventStatus')
        .getItems();

      const performers = new Set();
      let nonRecursive = 0;
      let recursive = 0;
      let children = 0;

      for (const event of events) {
        if (event.superEvent === this.event.id && event.eventHoursSpecification === null) {
          nonRecursive++;
        } else if (event.superEvent !== this.event.id) {
          children++;
        } else {
          recursive++;
        }

        if (event.performer) { performers.add(event.performer); }
      }

      this.eventSummary = {
        nonRecursive: nonRecursive,
        recursive: recursive,
        children: children,
        performers: performers.size
      };
    }
  }
}
