import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { AdvancedTableDataResult } from '@universis/ngx-tables';
import { template } from 'lodash';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-timetable-dashboard-classes',
  templateUrl: './timetable-dashboard-classes.component.html'
})
export class TimetableDashboardClassesComponent implements OnInit, OnDestroy {

  public isLoading: boolean;
  public timetableEvent: any;
  public recordsTotal: any;
  public readonly tableConfigSrc = 'assets/lists/CourseClassInstructors/active.json';
  public readonly searchConfigSrc = 'assets/lists/CourseClassInstructors/search.active.json';
  private paramSubscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute, private _context: AngularDataContext) { }

  ngOnInit(): void {
    this.paramSubscription = this._activatedRoute.params.subscribe(async ({ timetable }) => {
      this.isLoading = true;
      this.timetableEvent = await this._context.model('TimetableEvents')
        .where('id').equal(timetable)
        .expand('academicPeriods')
        .getItem();
      this.isLoading = false;
    });
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  onTableLoading({ target = null } = {}) {
    // set default filter
    if (target && target.config) {
      const { organizer, academicYear, academicPeriods } = this.timetableEvent;
      let filter = `courseClass/department eq ${organizer} and courseClass/year eq ${academicYear}`;
      if (academicPeriods && academicPeriods.length === 1) {
        filter += ` and courseClass/period eq ${academicPeriods[0].id}`;
      }
      target.config.defaults.filter = filter;
    }
  }

  ngOnDestroy(): void {
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
  }
}
