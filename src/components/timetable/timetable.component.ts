import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AngularDataContext } from '@themost/angular';
import { ClientDataQueryable } from '@themost/client';
import { ModalService, LoadingService, ErrorService } from '@universis/common';
import { ActivatedTableService, AdvancedFilterValueProvider, AdvancedRowActionComponent, AdvancedSearchFormComponent, AdvancedTableComponent, AdvancedTableDataResult, AdvancedTableSearchComponent } from '@universis/ngx-tables';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'app-timetable',
  templateUrl: './timetable.component.html'
})
export class TimetableComponent implements OnInit, OnDestroy {

  public recordsTotal: any;
  private dataSubscription: Subscription;
  private selectedItems = [];
  @Input() tableConfigSrc: any;
  @Input() searchConfigSrc: any;
  @ViewChild('table') table: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  @ViewChild('advancedSearch') advancedSearch: AdvancedTableSearchComponent;
  @Output() refreshAction: EventEmitter<any> = new EventEmitter<any>();

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _activatedTable: ActivatedTableService,
    private _translateService: TranslateService,
    private _modalService: ModalService,
    private _loadingService: LoadingService,
    private _errorService: ErrorService,
    private _context: AngularDataContext,
    private _advancedFilterValueProvider: AdvancedFilterValueProvider
  ) { }

  ngOnInit() {
    this.dataSubscription = this._activatedRoute.data.subscribe(data => {
      this._activatedTable.activeTable = this.table;
      if (!this._advancedFilterValueProvider.values.hasOwnProperty('currentDepartment')) {
        this._advancedFilterValueProvider.values['currentDepartment'] = data.organizer;
      }

      if (data.searchConfiguration) {
        // set search form source
        this.searchConfigSrc = data.searchConfiguration

        // TODO: This should be handled in ngx-tables. Advanced search form component should have local translations
        this.search.formComponent.formLoad.subscribe((form) => {
          if (form && Object.keys(form).length === 0) {
            return;
          }
          const currentLang = this._translateService.currentLang;
          const translation = this._translateService.instant('Events.Statuses');

          // overwrite Forms translations for event status with local translations
          Object.assign(this.search.formComponent.formio.i18next.options.resources[currentLang].translation, translation);
        });
      }

      // set table config source
      if (data.tableConfiguration) {
        this.tableConfigSrc = data.tableConfiguration
      }
    });
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
  }

  async getSelectedItems() {
    let items = [];
    if (this.table && this.table.lastQuery) {
      const lastQuery: ClientDataQueryable = this.table.lastQuery;
      if (lastQuery != null) {
        if (this.table.smartSelect) {
          // get items
          const selectArguments = ['id'];
          // query items
          const queryItems = await lastQuery.select.apply(lastQuery, selectArguments)
            .take(-1)
            .skip(0)
            .getItems();
          if (this.table.unselected && this.table.unselected.length) {
            // remove items that have been unselected by the user
            items = queryItems.filter(item => {
              return this.table.unselected.findIndex((x) => {
                return x.id === item.id;
              }) < 0;
            });
          } else {
            items = queryItems;
          }
        } else {
          // get selected items only
          items = this.table.selected.map((item) => {
            return {
              id: item.id
            };
          });
        }
      }
    }
    return items;
  }

  async deleteTimetable() {
    try {
      this._loadingService.showLoading();
      this.selectedItems = await this.getSelectedItems();
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'TimetableEvents.DeleteAction.Title',
          description: 'TimetableEvents.DeleteAction.Description',
          refresh: this.refreshAction,
          execute: this.executeDeleteTimetable()
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  executeDeleteTimetable() {
    return new Observable((observer) => {
      const total = this.selectedItems.length;
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      // get values from modal component
      this.refreshAction.emit({
        progress: 1
      });
      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            await this._context.model(`TimetableEvents`).remove(item);
            result.success += 1;
          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }
        }
        try {
          await this.table.fetch(true);
        } catch (err) {
          //
        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }
}
