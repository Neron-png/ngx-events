import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-timetable-root',
  templateUrl: './timetable-root.component.html'
})
export class TimetableRootComponent implements OnInit, OnDestroy {

  public editUrl;
  public model: any;
  private paramSubscription: Subscription;

  constructor(
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private _context: AngularDataContext
  ) { }

  ngOnInit() {
    this.paramSubscription = this._activatedRoute.params.subscribe(async ({ timetable }) => {
      this.editUrl = this._router.createUrlTree(['edit'], { relativeTo: this._activatedRoute }).toString();
      this.model = await this._context.model('TimetableEvents')
        .where('id').equal(timetable)
        .select('id,organizer/name as department,eventStatus,startDate,endDate,name')
        .getItem();
    });
  }

  ngOnDestroy(): void {
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
  }

}
