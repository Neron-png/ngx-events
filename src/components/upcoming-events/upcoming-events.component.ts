import {Component, Injector, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {EventsService, Model} from '../../events.service';
import {DateFormatter} from '../../advanced-date.formatter';
import {Router} from '@angular/router';

@Component({
  selector: 'universis-upcoming-events',
  templateUrl: './upcoming-events.component.html'
})
export class UpcomingEventsComponent implements OnInit, OnChanges {

  public events: Array<any>;
  public isLoading: boolean;
  @Input() model: Model;
  @Input() timetable: number | string;
  public _model = Model.Instructor;

  constructor(private _eventsService: EventsService,
              private _injector: Injector) {
  }

  async ngOnInit() {
    // get upcoming events and format their color
    this.isLoading = true;
    this.events = await this.getUpcomingEvents();
    this.events.forEach(event => event.color = DateFormatter.stringToColor(event.name));
    this.isLoading = false;
  }

  async ngOnChanges(changes: SimpleChanges) {
    if (changes.timetable) {
      if (changes.timetable.currentValue == null) {
        this.events = null;
        return;
      }
      this.isLoading = true;
      this.events = await this.getUpcomingEvents();
      this.isLoading = false;
    }
  }

  getUpcomingEvents() {
    const upcomingEventsQuery = this._eventsService.getUpcomingEvents(this.model);

    if (this.timetable) {
      // filter all "individual" (non-recursive and children of recursive) events associated with this timetable
      upcomingEventsQuery.where('superEvent').equal(this.timetable)
        .and('eventHoursSpecification').equal(null)
        .or('superEvent/superEvent').equal(this.timetable);
    } else {
      // otherwise filter all "individual" events
      upcomingEventsQuery.where('eventHoursSpecification').equal(null)
        .or('superEvent/additionalType').equal({ '$name': '$it/additionalType' });
    }

    return upcomingEventsQuery
      .expand('courseClass')
      .orderBy('startDate asc')
      .thenBy('courseClass/title')
      .take(3)
      .getItems();
  }

  goToURL(event: any) {
    if (event && event.courseClass && event.courseClass.course && event.id) {
      const router: Router = this._injector.get(Router);
      return router.navigate([
        '/courses',
        event.courseClass.course,
        event.courseClass.year,
        event.courseClass.period,
        'teachingEvents',
        event.id,
        'attendance'
      ]);
    }

  }
}
