  // tslint:disable: quotemark
  // tslint:disable: max-line-length
export const EVENT_ABSENCES_CONFIGURATION = {
  "searchExpression": "((indexof(student/person/familyName, '${text}') ge 0 or indexof(student/person/givenName, '${text}') ge 0 or indexof(student/studentIdentifier, '${text}') ge 0) and (student/studentStatus/alternateName eq 'active'))",
  "selectable": false,
  "multipleSelect": true,
  "columns": [
    {
      "name": "id",
      "property": "id",
      "hidden": true
    },
    {
      "name": "student",
      "property": "student",
      "hidden": true
    },
    {
      "name": "student/person/familyName",
      "property": "familyName",
      "title": "Last Name"
    },
    {
      "name": "student/person/givenName",
      "property": "givenName",
      "title": "Student Name"
    },
    {
      "name": "student/person/fatherName",
      "property": "fatherName",
      "title": "Father Name"
    },
    {
      "name": "student/studentIdentifier",
      "property": "studentIdentifier",
      "title": "Student Identifier"
    },
    {
      "name": "registration/semester",
      "property": "registrationSemester",
      "title": "RegistrationSemester"
    },
    {
      "name": "attendances",
      "property": "attendanceRecords",
      "virtual": true,
      "sortable": false,
      "title": "NumberOfAbsences",
      "className": "text-center",
      "formatters": [
        {
          "formatter": "TemplateFormatter",
          "formatString": "${attendances && Array.isArray(attendances) ?'<span class=\"text-center\">' + attendances.filter(x=> x.additionalType === 'TeachingEventAbsence').reduce((a,b) => a + b.coefficient ,0) + '</span>':'-'}"
        }
      ]
    },
    {
      "name": "status",
      "property": "status",
      "title": "StudentClassStatus",
      "sortable": true,
      "formatters": [
        {
          "formatter": "TemplateFormatter",
          "formatString": "${status.name}"
        }
      ]
    },
    {
      "name": "attendanceButton",
      "title": "Events.Absent",
      "sortable": false,
      "formatter": "AbsenceFormatter",
      "virtual": true,
      "className": "text-right"
    }
  ],
  "defaults": {
    "filter": "student/studentStatus/alternateName eq 'active'",
    "expand": "status",
    "orderBy": "student/person/familyName asc"
  },
  "criteria": [
    {
      "name": "studentGivenName",
      "filter": "indexof(student/person/givenName, '${value}') ge 0",
      "type": "text"
    },
    {
      "name": "studentFamilyName",
      "filter": "indexof(student/person/familyName, '${value}') ge 0",
      "type": "text"
    },
    {
      "name": "studentIdentifier",
      "filter": "indexof(student/uniqueIdentifier, '${value}') ge 0 or indexof(student/studentIdentifier, '${value}') ge 0 or indexof(student/studentInstituteIdentifier, '${value}') ge 0",
      "type": "text"
    },
    {
      "name": "status",
      "filter": "status eq '${value}'",
      "type": "text"
    }
  ]
};
