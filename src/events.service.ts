import { DatePipe } from '@angular/common';
import { Injectable } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import {AngularDataContext} from '@themost/angular';
import { ErrorService, ModalService, ToastService } from '@universis/common';
import { Observable } from 'rxjs';
import { NewEventComponent } from './components/new-event/new-event.component';
import { template } from 'lodash';
import * as moment_ from 'moment';
import { EmptyValuePostProcessor } from '@universis/forms';
const moment = moment_;

export const enum Model {
  Instructor = 'instructors/me/teachingEvents',
  Student = 'students/me/teachingEvents'
}

interface NewEventOptions {
  formProperties?: any;
  formData?: any;
  formSrc?: string;
  continueNavigation?: {
    continueLink: string,
    cancelLink?: string,
    navigationExtras?: NavigationExtras
  };
  execute?(submissionData: any, component: NewEventComponent): Promise<any> | any;
  afterExecute?(submittedData: any): Promise<any> | any;
}

@Injectable()
export class EventsService {

  constructor(
    private router: Router,
    private _errorService: ErrorService,
    private _modalService: ModalService,
    private _toastService: ToastService,
    private _context: AngularDataContext,
    private _translateService: TranslateService) {}


  getUpcomingEvents(model: Model | string) {
    return this._context.model(model)
      .asQueryable()
      .where('startDate')
      .greaterOrEqual((new Date()).toISOString())
      .prepare();
  }

  newEvent({
    formProperties = {},
    formData = {},
    formSrc = 'TeachingEvents/newSimple',
    continueNavigation,
    execute,
    afterExecute
  }: NewEventOptions = {}) {

    // default execute function
    if (typeof execute !== 'function') {
      execute = (submissionData) => this._context.model('Events').save(submissionData);
    }

    // default afterExecute function
    if (typeof afterExecute !== 'function') {
      afterExecute = () => this._toastService.show(
        this._translateService.instant('Events.CreatedNew'),
        this._translateService.instant('Events.CreatedSuccess'),
        true,
        3000);
    }

    // configure a navigation function which will be called when the modal closes
    const navigate = () => {
      const nav = (link, data = {}) => {
        link = continueNavigation && (continueNavigation[link] || continueNavigation.continueLink);
        if (link) {
          link = template(link)(data);
          this.router.navigate([link], continueNavigation.navigationExtras);
        }
      };
      return {
        cancel: (data) => nav('cancelLink', data),
        continue: (data) => nav('continueLink', data)
      };
    };

    //  open new event component as modal
    try {
      this._modalService.openModalComponent(NewEventComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          formProperties,
          formSrc,
          formData,
          navigate,
          execute: (() => {
            return new Observable((observer) => {
              const component = <NewEventComponent>this._modalService.modalRef.content;

              (async () => {
                try {
                  // convert empty objects and strings to null
                  new EmptyValuePostProcessor().parse(component.formComponent.form.config, component.formComponent.form.formio.data);
                  // make a deep copy of the form's data so that it isn't modified in execute()
                  const result = await execute(JSON.parse(JSON.stringify(component.formComponent.form.formio.data)), component);
                  observer.next();
                  try {
                    afterExecute(result);
                  } catch (error) {
                    // pass: errors inside afterExecute should be handled independently
                  }
                } catch (error) {
                  observer.error(error);
                }
              })();
            });
          })()
        }
      });
    } catch (err) {
      console.log(err);
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  parseFormData(formData, { courseClass = null, instructor = null, recursive = false } = {}) {
    const data = JSON.parse(JSON.stringify(formData));
    const datePipe: DatePipe = new DatePipe('el');
    if (courseClass) {
      data['about'] = courseClass && courseClass['id'];
      data['courseClass'] = courseClass && courseClass.hasOwnProperty('id') && courseClass['id'];
      data['organizer'] = courseClass && courseClass.hasOwnProperty('course') &&
        courseClass['course'].hasOwnProperty('department') &&
        courseClass['course']['department'];
    }
    if (instructor) {
      data['performer'] = instructor && instructor.hasOwnProperty('user') && instructor['user'];
    }
    data['inLanguage'] = null;
    data['name'] = data['title'];
    delete data['title'];
    if (recursive) {
      const duration = moment.duration(moment(data.endTime).diff(moment(data.startTime))).toISOString();
      const start = parseInt(datePipe.transform(data.startDate, 'M'), 10);
      const end = parseInt(datePipe.transform(data.endDate, 'M'), 10);
      const months = [];
      for (let i = start; i <= end; i += 1) {
        months.push(i);
      }
      const _months = months.join(',');
      const days = data.pickDay.join(',');
      const validFrom = data.startDate = moment(data.startDate).hours(moment(data.startTime).hours()).minutes(moment(data.startTime).minutes()).toDate();
      const validThrough = data.endDate = moment(data.endDate).hours(moment(data.endTime).hours()).minutes(moment(data.endTime).minutes()).toDate();
      data['eventHoursSpecification'] = {
        'duration': duration,
        'validFrom': validFrom,
        'validThrough': validThrough,
        'opens': datePipe.transform(data.startTime, 'HH:mm:ss'),
        'closes': datePipe.transform(data.endTime, 'HH:mm:ss'),
        'minuteOfHour': datePipe.transform(data.startTime, 'mm'),
        'hourOfDay': datePipe.transform(data.startTime, 'HH'),
        'dayOfMonth': '*',
        'monthOfYear': _months,
        'dayOfWeek': days
      };
      data['duration'] = duration;
      data['contributor'] = null;
      data['isAccessibleForFree'] = true;
      data['contributor'] = null;
      delete data.pickDay;
    } else {
      data['eventHoursSpecification'] = null;
      data['duration'] = null;
      data['contributor'] = null;
      data['isAccessibleForFree'] = true;
      data['contributor'] = null;
      data['startDate'] = data.startDate = moment(data.startDate).hours(moment(data.startTime).hours()).minutes(moment(data.startTime).minutes()).toDate();
      data['endDate'] = data.endDate = moment(data.startDate).hours(moment(data.endTime).hours()).minutes(moment(data.endTime).minutes()).toDate();
      data['eventAttendanceCoefficient'] = parseInt(data['eventAttendanceCoefficient']);
      delete data.input;
      delete data.startTime;
      delete data.endTime;
    }
    return [data];
  }
}
