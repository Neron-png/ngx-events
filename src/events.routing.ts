import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { DeleteEventComponent } from './components/delete-event/delete-event.component';
import { EventAbsencesComponent } from './components/event-absences/event-absences.component';
import { EventsLibComponent } from './components/events/events.component';
import { NewEventContainerComponent } from './components/new-event/new-event-container.component';
import { TimetableCopyComponent } from './components/timetable/timetable-copy.component';
import { TimetableDashboardClassesComponent } from './components/timetable/timetable-dashboard/timetable-dashboard-classes/timetable-dashboard-classes.component';
import { TimetableDashboardOverviewComponent } from './components/timetable/timetable-dashboard/timetable-dashboard-overview/timetable-dashboard-overview.component';
import { TimetableDashboardComponent } from './components/timetable/timetable-dashboard/timetable-dashboard.component';
import { TimetableHomeComponent } from './components/timetable/timetable-home/timetable-home.component';
import { TimetableNewEditComponent } from './components/timetable/timetable-new-edit/timetable-new-edit.component';
import { TimetableRootComponent } from './components/timetable/timetable-root/timetable-root.component';
import { TimetableComponent } from './components/timetable/timetable.component';
import { CourseClassInstructorResolver, TimetableTableConfigurationResolver, TimetableTableSearchResolver } from './components/timetable/timetable.resolver';
import { HttpClient } from '@angular/common/http';

export function CourseClassInstructorResolverFactory(context: AngularDataContext) {
  return new CourseClassInstructorResolver(context);
}

export function TableConfigSrcResolverFactory(httpClient: HttpClient) {
  return new TimetableTableConfigurationResolver(httpClient);
}

export function TableSearchSrcResolverFactory(httpClient: HttpClient) {
  return new TimetableTableSearchResolver(httpClient);
}

const routes: Routes = [
  {
    path: '',
    component: TimetableHomeComponent,
    data: {
      model: 'TimetableEvents'
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'list/open'
      },
      {
        path: 'list',
        pathMatch: 'full',
        redirectTo: 'list/open'
      },
      {
        path: 'list/:list',
        component: TimetableComponent,
        resolve: {
          tableConfiguration: TimetableTableConfigurationResolver,
          searchConfiguration: TimetableTableSearchResolver
        }
      }
    ]
  },
  {
    path: 'create',
    component: TimetableRootComponent,
    data: {
      model: 'TimetableEvents'
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'new'
      },
      {
        path: 'new',
        component: TimetableNewEditComponent
      }
    ]
  },
  {
    path: ':timetable',
    component: TimetableRootComponent,
    data: {
      model: 'TimetableEvents'
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'dashboard'
      },
      {
        path: 'dashboard',
        component: TimetableDashboardComponent,
        children: [
          {
            path: '',
            pathMatch: 'full',
            redirectTo: 'overview'
          },
          {
            path: 'overview',
            component: TimetableDashboardOverviewComponent,
            data: {
              title: 'TimetableEvents.Overview'
            },
            children: [
              {
                path: 'copy',
                component: TimetableCopyComponent,
                data: {
                  model: 'TimetableEvents',
                  action: 'copy'
                }
              }
            ]
          },
          {
            path: 'classes',
            component: TimetableDashboardClassesComponent,
            data: {
              title: 'Tables.Classes',
              model: 'TeachingEvent'
            },
            children: [
              {
                path: ':id/add',
                pathMatch: 'full',
                component: NewEventContainerComponent,
                data: {
                  model: 'TeachingEvents',
                  action: 'newSimple'
                },
                resolve: {
                  data: CourseClassInstructorResolver
                }
              },
              {
                path: ':id/addRecursive',
                pathMatch: 'full',
                component: NewEventContainerComponent,
                data: {
                  model: 'TeachingEvents',
                  action: 'newRecursive',
                  recursive: true
                },
                resolve: {
                  data: CourseClassInstructorResolver
                }
              }
            ]
          },
          {
            path: 'events/list',
            component: EventsLibComponent,
            data: {
              title: 'Tables.Events',
              model: 'TeachingEvents',
              showActions: false, // TODO: change it to true when actions are fixed (client & API)
              searchConfigSrc: 'assets/lists/TeachingEvents/search.all.json',
              tableConfigSrc: 'assets/lists/TeachingEvents/all.json'
            },
            children: [
              {
                path: ':id/delete',
                component: DeleteEventComponent,
                data: {
                  _model: 'TeachingEvents/${id}',
                }
              }
            ]
          },
          {
            path: 'events/:id',
            data: {
              _model: 'TeachingEvents/${id}/attendance',
              eventModel: 'TeachingEvents',
              back: false
            },
            component: EventAbsencesComponent
          }
        ]
      },
      {
        path: 'edit',
        component: TimetableNewEditComponent,
        data: {
          action: 'edit',
          serviceQueryParams: {
            $expand: 'availablePlaces,availableEventTypes($select=name,alternateName,id),academicPeriods($select=name,id),examPeriods($select=name,id),academicYear',
          },
          continue: '/events/${id}/dashboard/overview'
        },
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: [
  ],
  providers: [
    {
      provide: TimetableTableConfigurationResolver,
      useFactory: TableConfigSrcResolverFactory,
      deps: [HttpClient]
    },
    {
      provide: TimetableTableSearchResolver,
      useFactory: TableSearchSrcResolverFactory,
      deps: [HttpClient]
    },
    {
      provide: CourseClassInstructorResolver,
      useFactory: CourseClassInstructorResolverFactory,
      deps: [AngularDataContext]
    }
  ]
})
export class EventsRoutingModule {
}
