// tslint:disable: no-bitwise
import { AdvancedColumnFormatter } from '@universis/ngx-tables';
import {DatePipe} from '@angular/common';
import {ConfigurationService} from '@universis/common';
export class DateFormatter extends AdvancedColumnFormatter {
    static stringToColor(str) {
      let i;
      if (str == null) {
        str = 'placeholder';
      }
      let hash = 0;
      for (i = 0; i < str.length; i++) {
        hash = str.charCodeAt(i) + ((hash << 5) - hash);
      }
      let colour = '#';
      for (i = 0; i < 3; i++) {
        const value = (hash >> (i * 8)) & 0xFF;
        colour += ('00' + value.toString(16)).substr(-2);
      }
      return colour;
    }

    static LightenDarkenColor(col, amt) {
      function boundVal(val) {
        if (val > 255) { return 255; }
        if (val < 0) { return 0; }
        return val;
      }

      let usePound = false;
      if (col[0] === '#') {
        col = col.slice(1);
        usePound = true;
      }

      const num = parseInt(col, 16);

      let r = (num >> 16) + amt;
      let b = ((num >> 8) & 0x00FF) + amt;
      let g = (num & 0x0000FF) + amt;

      r = boundVal(r);
      b = boundVal(b);
      g = boundVal(g);

      return (usePound ? '#' : '') + (g | (b << 8) | (r << 16)).toString(16);
    }

    render(data, type, row, meta) {
      // get column
      const column = meta.settings.aoColumns[meta.col];
      if (column && column.data) {
        const currLocale = this.injector.get(ConfigurationService).currentLocale;
        const datePipe: DatePipe = new DatePipe(currLocale);
        const date = datePipe.transform(data, 'EEE d MMM,');
        const dot = DateFormatter.stringToColor(row.name);
        const time = datePipe.transform(data, 'HH:mm') + ' - ' + datePipe.transform(row.endDate, 'HH:mm');

        // tslint:disable-next-line: max-line-length
        return ` <span class="px-1" style="height: 13px; width: 13px; background-color: ${dot}; border-radius: 50%; display: inline-block;"></span>
        <span class="pl-2">${date} ${time}</span>`;
      }
      return data;
    }

    constructor( private configurationService: ConfigurationService) {
      super();
    }
  }
