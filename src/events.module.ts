import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { COLUMN_FORMATTERS, FORMATTERS, TablesModule } from '@universis/ngx-tables';
import { ConfigurationService, SharedModule } from '@universis/common';
import { EventsLibComponent } from './components/events/events.component';
import { NewEventComponent } from './components/new-event/new-event.component';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { AdvancedFormsModule } from '@universis/forms';
import { AbsencesComponent } from './components/absences/absences.component';
import { ModalService } from '@universis/common';
import { environment } from './environments/environment';
import { EVENTS_LOCALES } from './components/i18n';
import { EventAbsencesComponent } from './components/event-absences/event-absences.component';
import { RouterModalModule } from '@universis/common/routing';
import { UpcomingEventsComponent } from './components/upcoming-events/upcoming-events.component';
import { RouterModule } from '@angular/router';
import { CalendarA11y, CalendarDateFormatter, CalendarEventTitleFormatter, CalendarModule, CalendarUtils, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { BsModalService, ComponentLoaderFactory, ModalModule, PositioningService, ProgressbarConfig, ProgressbarModule, TooltipConfig, TooltipModule } from "ngx-bootstrap";
import { FormioAppConfig } from "angular-formio";
import { DeleteEventComponent } from './components/delete-event/delete-event.component';
import { TimetableComponent } from './components/timetable/timetable.component';
import { EventsRoutingModule } from './events.routing';
import { TimetableHomeComponent } from './components/timetable/timetable-home/timetable-home.component';
import { TimetableDashboardComponent } from './components/timetable/timetable-dashboard/timetable-dashboard.component';
import { TimetableRootComponent } from './components/timetable/timetable-root/timetable-root.component';
import { TimetableDashboardOverviewComponent } from './components/timetable/timetable-dashboard/timetable-dashboard-overview/timetable-dashboard-overview.component';
import { NewEventContainerComponent } from './components/new-event/new-event-container.component';
import { TimetableDashboardClassesComponent } from './components/timetable/timetable-dashboard/timetable-dashboard-classes/timetable-dashboard-classes.component';
import { AbsenceFormatter } from './advanced-absence.formatter';
import { StatusFormatter } from './advanced-status.formatter';
import { DateFormatter } from './advanced-date.formatter';
import { TimetableDashboardOverviewGeneralComponent } from './components/timetable/timetable-dashboard/timetable-dashboard-overview/timetable-dashboard-overview-general/timetable-dashboard-overview-general.component';
import { TimetableDashboardOverviewStatsComponent } from './components/timetable/timetable-dashboard/timetable-dashboard-overview/timetable-dashboard-overview-stats/timetable-dashboard-overview-stats.component';
import { TimetableNewEditComponent } from './components/timetable/timetable-new-edit/timetable-new-edit.component';
import { EventsService } from './events.service';
import { NewEventRoutedComponent } from './components/new-event-routed/new-event-routed.component';
import { TimetableDashboardOverviewActionsComponent } from './components/timetable/timetable-dashboard/timetable-dashboard-overview/timetable-dashboard-overview-actions/timetable-dashboard-overview-actions.component';
import { TimetableCopyComponent } from './components/timetable/timetable-copy.component';
import { NgStringPipesModule } from 'ngx-pipes';
import { HttpClientModule } from '@angular/common/http';


export function ExtendedColumnsFactory(configService: ConfigurationService) {
  return {
    ...FORMATTERS,
    DateFormatter: new DateFormatter(configService),
    AbsenceFormatter: new AbsenceFormatter(),
    StatusFormatter: new StatusFormatter(configService)
  }
}

@NgModule({
  imports: [
    HttpClientModule,
    NgStringPipesModule,
    CommonModule,
    FormsModule,
    TranslateModule,
    TablesModule,
    SharedModule,
    AdvancedFormsModule,
    RouterModalModule,
    RouterModule,
    CalendarModule,
    ModalModule,
    TooltipModule,
    ProgressbarModule,
    EventsRoutingModule
  ],
  declarations: [
    EventsLibComponent,
    NewEventComponent,
    NewEventContainerComponent,
    NewEventRoutedComponent,
    AbsencesComponent,
    EventAbsencesComponent,
    UpcomingEventsComponent,
    DeleteEventComponent,
    TimetableComponent,
    TimetableHomeComponent,
    TimetableDashboardComponent,
    TimetableRootComponent,
    TimetableDashboardOverviewComponent,
    TimetableDashboardClassesComponent,
    TimetableDashboardOverviewGeneralComponent,
    TimetableDashboardOverviewStatsComponent,
    TimetableNewEditComponent,
    TimetableDashboardOverviewActionsComponent,
    TimetableCopyComponent
  ],
  exports: [
    EventsLibComponent,
    UpcomingEventsComponent,
    DeleteEventComponent
  ],
  providers: [
    TooltipConfig,
    BsModalService,
    ComponentLoaderFactory,
    PositioningService,
    EventsService,
    ModalService,
    {
      provide: DateAdapter,
      useFactory: adapterFactory
    },
    CalendarEventTitleFormatter,
    CalendarDateFormatter,
    CalendarUtils,
    CalendarA11y,
    ProgressbarConfig,
    {
      provide: FormioAppConfig,
      useValue: {}
    },
    {
      provide: COLUMN_FORMATTERS,
      useFactory: ExtendedColumnsFactory,
      deps: [ConfigurationService]
    }
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  entryComponents: [
    NewEventComponent, EventsLibComponent, UpcomingEventsComponent, DeleteEventComponent
  ]
})

export class EventsModule {
  constructor( private _translateService: TranslateService) {
    this.ngOnInit();
}
  // tslint:disable-next-line:use-life-cycle-interface
  ngOnInit() {
    environment.languages.forEach( language => {
        if (EVENTS_LOCALES.hasOwnProperty(language)) {
            this._translateService.setTranslation(language, EVENTS_LOCALES[language], true);
        }
    });
  }
}
